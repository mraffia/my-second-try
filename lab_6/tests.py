from django.test import TestCase

# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_lab_6_url_exists(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve('/lab-6/')
        self.assertEqual(found.func, index)
